# StatusPageClient

This is an API client for accessing the status page API.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'status_page_client'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install status_page_client

## Usage

```
# Create a new client object, providing URL and auth token:
client = StatusPage::Client.new(auth_token: "af85baa58b6f8", base_url: "http://localhost:3000")

# Fetch the current status:
client.current_status
=> #<struct StatusPage::Client::CurrentStatus message="All issues resolved.", status=nil>

# Update the status:
client.update_status(message: "All issues resolved", status: "UP")
=> true

# Failed requests, or incorrect will raise a `StatusPage::ApiError`:
client = StatusPage::Client.new(auth_token: "BAD", base_url: "http://localhost:3000")
client.update_status(message: "All issues resolved", status: "UP")
=> StatusPage::ApiError: {:response=>#<Net::HTTPUnauthorized 401  readbody=true>}
```

## Development

After checking out the repo, run `bundle install` to install dependencies.
Then, run `bundle exec rspec` to run the tests.
