require 'json'

module StatusPage
  class Client
    CurrentStatus = Struct.new(:message, :status)

    def initialize(auth_token: nil, base_url:)
      @auth_token = auth_token
      @base_url = base_url
    end

    def current_status
      uri = URI.parse("#{@base_url}/api/v1/current_status")
      response = Net::HTTP.get_response(uri)

      ensure_success!(response)

      parsed_response = JSON.parse(response.body)
      return CurrentStatus.new(parsed_response["message"], parsed_response["status"])
    end

    def update_status(status: nil, message: nil)
      uri = URI.parse("#{@base_url}/api/v1/status_updates")

      request = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
      request.body = { status: status, message: message }.compact.to_json
      request.add_field("Authorization", "Bearer #{@auth_token}")

      response = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(request)
      end

      ensure_success!(response)

      return true
    end

    private

    def ensure_success!(response)
      unless response.code.to_i >= 200 && response.code.to_i < 300
        raise ApiError.new({response: response})
      end
    end
  end
end
