require 'spec_helper'
require 'status_page'

RSpec.describe StatusPage::Client do
  it "has a version number" do
    expect(StatusPage::VERSION).to_not be nil
  end

  describe "#current_status" do
    it "returns current status & message" do
      stub_request(:get, "http://localhost:3000/api/v1/current_status")
        .to_return(status: 200, body: {status: "UP", message: "All issues resolved."}.to_json)

      client = StatusPage::Client.new(base_url: "http://localhost:3000")
      expect(client.current_status.status).to eq "UP"
      expect(client.current_status.message).to eq "All issues resolved."
    end

    it "handles nil values" do
      stub_request(:get, "http://localhost:3000/api/v1/current_status")
        .to_return(status: 200, body: {status: nil, message: "All issues resolved."}.to_json)

      client = StatusPage::Client.new(base_url: "http://localhost:3000")
      expect(client.current_status.status).to eq nil
      expect(client.current_status.message).to eq "All issues resolved."
    end
  end

  describe "#update_status" do
    before do
      stub_request(:post, "http://localhost:3000/api/v1/status_updates").
        to_return(status: 401)

      stub_request(:post, "http://localhost:3000/api/v1/status_updates").
        with(
          body: { status: "UP", message: "All issues resolved" }.to_json,
          headers: { 'Authorization'=>'Bearer af85baa58b6f8' }).
        to_return(status: 200)
    end

    it "allows updating of the status" do
      client = StatusPage::Client.new(auth_token: "af85baa58b6f8", base_url: "http://localhost:3000")
      expect(client.update_status(message: "All issues resolved", status: "UP")).to eq true
    end

    it "raises ApiError in case of failed auth" do
      client = StatusPage::Client.new(auth_token: "fake", base_url: "http://localhost:3000")
      expect { client.update_status(message: "All issues resolved", status: "UP") }.to raise_error(StatusPage::ApiError)
    end
  end
end
